# Italian Linux Society - materiali

Raccolta di materiali liberamente utilizzabili per l'attività associativa Italian Linux Society e la promozione del software libero in generale.

## Images

### Logo Tux Clean

Logo tux, in SVG:

![logo tux](images/logo/italian_linux_society_penguin_circle_logo.svg)

Logo tux, in SVG and PNG:

https://commons.wikimedia.org/wiki/File:Linux_tux_circle_logo.svg

Logo tux, credits:

> Logo tux by Italian Linux Society, Roberto Guido, restyled by Virginia Foti, CC0.

### Logo Tux Loving Linux.it

Logo tux love Linux.it, in SVG:

![logo tux loving linux.it](images/promo/tux_love_linux_it.svg)

Printing tips:

- circle pins 1.5"
- circle stickers 2″
- circle stickers 3″

Logo tux, credits:

> Logo tux by Italian Linux Society, Roberto Guido, restyled by Virginia Foti, emojii+text by Valerio Bozzolan, CC0.

### Logo Italian Linux Society

Logo Italian Linux Society, in SVG:

![logo ILS](images/logo/italian_linux_society_penguin_circle_logo_complete.svg)

Logo Italian Linux Society, in SVG and PNG:

https://commons.wikimedia.org/wiki/File:Italian_Linux_Society_logo_text_penguin_circle_black.svg

Logo Italian Linux Society, credits:

> Logo Italian Linux Society by Virginia Foti, 2022. Penguin originally designed by Roberto Guido. CC0.

### Logo Usa Analizza Modifica Condividi

Logo Usa Analizza Modifica Condividi, SVG:

![usa analizza modifica condividi](images/promo/usa_analizza_modifica_condividi.svg)

Print tips:

```
Adesivi per interni
rotondo 4,5 cm
80 g carta adesiva bianca
```

Logo Usa Analizza Modifica Condividi, credits:

> Logo Usa Analizza Modifica Condividi, by Italian Linux Society, Roberto Guido. CC0.

### Logo Linux.it traditional

Logo Linux.it traditional, JPG:

![Linux.it](images/promo/linux_it_traditional.jpg)

Print tips:

```
Adesivi per interni
rotondo 3 cm
80 g carta adesiva bianca
```

Logo Linux.it credits:

> Logo Linux.it, Italian Linux Society, Roberto Guido. CC0

### Logo LinuxSi.com

![logo LinuxSi.com](images/promo/linuxsi.jpg)

> Logo LinuxSi.com, Italian Linux Society, Roberto Guido. CC0

Print tips:

```
Adesivi per esterni
rotondo 7 cm
90 µm pellicola adesiva bianca (senza intaglio sul retro)
```

## Liberate Questo Software

Questo è un invito a rimuovere determinate restrizioni, limitazioni o licenze proprietarie da un certo software.
Qual è il software in questione? Probabilmente la cosa è evidente dal contesto in cui posizioni questo adesivo,
oppure potrai chiarirlo tu, a voce, ai tuoi interlocutori incuriositi.

![liberate_questo_software_pinguino_linuxit_topfirm.png](images/promo/liberate_questo_software_pinguino_linuxit_topfirm.png)

Questa grafica è pensata per adesivi rettangolari 75x50mm. Altri formati:

* [liberate_questo_software_pinguino_linuxit_topfirm.pdf](images/promo/liberate_questo_software_pinguino_linuxit_topfirm.pdf)
* [liberate_questo_software_pinguino_linuxit_topfirm.svg](images/promo/liberate_questo_software_pinguino_linuxit_topfirm.svg)

Versioni alternative:

* [liberate_questo_software_pinguino_linuxit_bellyfirm.pdf](images/promo/liberate_questo_software_pinguino_linuxit_bellyfirm.pdf)

Crediti suggeriti:

> Logo "Liberate Questo Software", ottobre 2024, by Valerio Bozzolan, Italian Linux Society, contributors, [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
>
> Font "Anton" by Vernon Adams, [SIL OPEN FONT LICENSE 1.1](https://openfontlicense.org/)

### Brochure Italian Linux Society

Brochure Italian Linux Society, SVG (da aprire con Inkscape con i font giusti per vedere i testi):

![brochure Italian Linux Society](images/promo/brochure_ils.svg)

Brochure Italian Linux Society, crediti:

> Brochure Italian Linux Society by Roberto Guido. CC0.

Consigli di stampa:

```
Pieghevoli, piega a portafoglio
DIN lungo (9,8 x 21 cm)
135 g carta patinata opaca PEFC™
```

## Slide per Presentazioni

Per una presentazione con LibreOffice, ecco alcune (brevi) slide:

https://www.ils.org/sostieni/slides/

Abbiamo un template in formato ODP a tema ILS: https://gitlab.com/ItalianLinuxSociety/Materiali/-/blob/master/visual-identity/template-ils.odp

## Printables

### Quando Cucino

Locandina stampabile "Quando Cucino", A4, a colori e in B/N, in vari formati.

Anteprima:

![locandina quando cucino software libero](manifests/quando_cucino/original/quando_cucino.png)

* [quando cucino.pdf](manifests/quando_cucino/original/quando_cucino.png)
* [quando cucino da colorare.pdf](manifests/quando_cucino/black_white/quando_cucino_bw.pdf)
* [quando cucino tutti i formati - PDF, SVG, PNG, KRA...](manifests/quando_cucino)

> [Marco "Zughy" Amato](https://mastodon.social/@zughy_boi), ottobre 2023, CC0.

### Crosswords Italian Linux Society v1

Parole crociate A4 stampabile in B/N, in formato PDF e ODG:

* [crosswords A4 Italian Linux Society.pdf](manifests/a4_crosswords_ils_1.pdf)
* [crosswords A4 Italian Linux Society.odg](manifests/a4_crosswords_ils_1.odg)

Crediti:

> Valerio Bozzolan, CC0.

## Licenza

Salvo ove diversamente indicato, tutti i contributi in questo repository (sia testo che immagini o altri materiali multimediali) sono pubblicati in pubblico dominio (Creative Commons Zero).

https://creativecommons.org/publicdomain/zero/1.0/
