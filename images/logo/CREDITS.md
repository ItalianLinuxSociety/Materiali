## Credits

### italian_linux_society_penguin_circle_logo_complete.svg

Author: Virginia Foti with parts from Roberto Guido.

Publication date: 2022-06-13

License: CC0.

### italian_linux_society_penguin_circle_logo.svg

Author: Roberto Guido.

Reworker: Virginia Foti.

Publication date: 2021.

License: CC0.

### italian_linux_society_penguin_tshirt_logo_old.svg

The original uploader was MadBob at Italian Wikipedia., [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons.

https://commons.wikimedia.org/wiki/File:Logoilsvettoriale.svg
