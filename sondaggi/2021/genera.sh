#!/usr/bin/env bash

./genera-pdf.py soci
./genera-pdf.py nonsoci
./genera-pdf.py completo
./genera-pdf.py 35sotto
./genera-pdf.py 35sopra
./genera-pdf.py etasociiscritti

echo "Analisi generazione in corso"
pandoc analisi.md -o analisi.pdf -V geometry:margin=0.5in -V 'mainfont:DejaVuSerif' --pdf-engine=xelatex
