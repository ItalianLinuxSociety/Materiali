# Template LateX per slide Linux Day

> Autore: Davide Isoardi [abacus@linux.it](mailto:abacus@linux.it)  
> Last update: 04 Ottobre 2024

## Dipendenze
Si compila con XeLaTeX (anche con LuaLaTeX ma non l'ho provato).  
Richiede che a sistema ci siano i font [Raleway](https://fonts.google.com/specimen/Raleway) (tutto) e [Rokkitt](https://fonts.google.com/specimen/Rokkitt) (titolo).
